#include <stdio.h>

int main()
{
	int n;

	printf("Please enter a number: ");
	scanf("%d", &n);
	printf("Factors are: ");

	for(int x=1;x<=n;x++)
	{
		if(n%x==0)
		{
			printf("%d ", x);
		}
	}

	return 0;
}
