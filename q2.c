#include <stdio.h>

int main()
{
	int x=2,prime=1,n;

	printf("Please enter a number: ");
	scanf("%d", &n);

	while(x<n)
	{
		if(n%x==0)
		{
			prime=0;
			break;
		}
		x++;
	}

	if(prime==0 || n==1)
	{
		printf("This is not a prime number.\n");
	}
	else
	{
		printf("This is a prime number.\n");
	}
	
}
