#include <stdio.h>

int main()
{
	int n,reversed_n=0;

	printf("Please enter a number: ");
	scanf("%d", &n);

	while(n!=0)
	{
		reversed_n= reversed_n*10 + n%10;
		n=n/10;
	}

	printf("Reversed number = %d\n", reversed_n);
	return 0;
}

